class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def hello
	render html: "Bonjour, mon amie!"
  end
end
